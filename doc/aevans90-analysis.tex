\documentclass[]{article}
\usepackage{graphicx}
\usepackage{subcaption}

%opening
\title{CS7641: Machine Learning \\
	Gatech OMSCS Program\\
	Supervised Learning Assignment}
\author{Ames Evans (aevans90)}

\begin{document}

\maketitle

\section{Introduction and Data Used}

This assignment asked me to explore some techniques in supervised learning. Throughout this document, I will explain how I learned about several supervised learning techniques, including:
\begin{itemize}
	\item Decision trees
	\item Neural networks
	\item Boosting
	\item Support Vector Machines
	\item k-nearest neighbors
\end{itemize}

In order to accomplish this, I largely relied upon scikit's helpful libraries and documentation. I was tempted to demonstrate the given algorithms on some of the easier datasets such as the iris or breast\_cancer sets included with scikit, but I opted to use a slightly more challenging set. The main data set I ended up using was the 20 Newsgroups dataset. 

I chose to use 20 newsgroups for my data because it has a larger set of testing examples than some of the other example datasets (about 18,000) split among 20 different topics. In addition, it presents some challenges related to pre-processing the data before using it to train a classification algorithm. Usually, this is accomplished by vectorizing the data into a number of times that each word appears in a post, which is what I did. This results in a very sparse vector that represents the number of occurrences of given words in the data. Another of the challenges present in the 20 Newsgroups data is overfitting. While this grim adversary lurks in the darkness behind any machine learning algorithm, it is particularly present in this dataset. This is because there are a lot of "useless" words present in a lot of the data that won't help with classification, and one has to be careful not to overtrain. Finally, I have a personal interest in the topic of data-mining text. I am looking forward to determining which supervised learning algorithms are best-suited to this dataset.

 For this assignment I ended up limiting the number of newsgroups to about 5, trying for a diverse selection from among the topics. I settled on using rec.autos, sci.crypt, talk.religion.misc, talk.politics.guns, and misc.forsale. I did this to make it potentially easier for my algorithms to distinguish between the different groups by avoiding particularly similar topics, like rec.autos and rec.motorcycles. For each of the techniques, I focused on testing using cross-validation. I found some really helpful code that segmented the training data and saved some for testing data, as well as generated graphs with useful metrics on the Scikit website. Using this, I was able to compare the learning curves and scaling properties of each different run as I added training examples. I could also see and how quickly the model was able to classify new examples as it grew in complexity. This is primarily how I compared the different techniques.

\section{Decision Trees}

The decision tree algorithm I chose was Scikit's built-in decision tree function. I used the Gini impurity criterion for deciding how to measure the quality of splits of the tree, and I used the "best" strategy for choosing when to split of each node. The Gini impurity is a measure of about how often a randomly chosen sample would be incorrectly labeled if the label was randomly chosen from among those in the data set.

In order to try and constrain my model from overfitting and using up all of my memory, I decided to limit the max depth of the tree to 10 deep, and I limited the number of leaf nodes to 30. The learning process took about 30 seconds to run. Here were my resulting learning curve and representation of my decision tree (Figure 1). 

\begin{figure}
	\centering
	\begin{subfigure}[b]{0.8\linewidth}
		\centering
		\includegraphics[width=0.8\linewidth]{images/dt10deepgraph.png}
		\caption{Decision Tree}
	\end{subfigure}
	\begin{subfigure}[b]{1.3\linewidth}
		\centering
		\includegraphics[width=\linewidth]{images/dt10deeplearningcurve.png}	
		\caption{Learning curve results}		
	\end{subfigure}
	\caption{Decision Tree learning, limited to depth of 10}

\end{figure}

After seeing these results, I realized that my decision tree classifier was only able to correctly classify about 0.55 of the validation samples. I suppose this isn't terrible, given that a random selection would give something like 0.25. I changed my maximum depth to 20 nodes to see whether it would get better (Figure 2), and it did, improving my cross-validation success rate to 0.6 or so. As for the other parts of Figures 1 and 2, it seemed like this model scaled pretty linearly with respect to the number of training examples, and its learning approached a fixed value pretty asymptotically. I think if I had used a more complex decision tree and allowed it to grow bigger, this may have had better performance. In the future, I would do some sort of a search across the space of decision tree complexity to try and find more optimal values of maximum leaf nodes to try and prevent overfitting, while also improving the finished model's accuracy.

\begin{figure}
	\centering
	\includegraphics[width=1.3\linewidth]{images/dt20deeplearningcurve.png}
	\caption{Decision Tree learning, limited to depth of 20}
\end{figure}



\section{Neural Network}

For the neural network algorithm, I again went with Scikit. I decided to use the MLPClassifier, which is a Multi-Layer Perceptron Classifier. This means that like what we covered in our lectures, it uses a network of perceptrons to learn to classify samples. Specifically, I chose to use the "lbfgs" algorithm, which stands for "limited-memory Broyden–Fletcher–Goldfarb–Shanno". Frankly, understanding exactly what this algorithm does is beyond me, although I skimmed the Wikipedia page explaining what it did. After a few attempts of tweaking this algorithm to do what I wanted, it frustratingly seemed to actually get worse over time! This was even after I pruned back my classification dataset to only two categories, instead of my original five. Some of the options I used on this classifier included a starting 'alpha' or learning rate of 1e-4, an "adaptive" learning rate that decayed when training loss stops decreasing. I tried this a few times, but the neural network actually seemed to get worse over time! I'm not sure what I was doing wrong, but I decided to revert to a tried-and-true algorithm I have used in another class, 'adam'. 

\begin{figure}
	\centering
	\includegraphics[width=1.3\linewidth]{images/MLPC10x10Adamlearningcurve.png}
	\caption{Neural Network learning using Adam and two 10-node hidden layers}
\end{figure}

When I used the 'adam' neural network technique, I had a lot more success. I am already somewhat familiar with this algorithm because it is what I used for my lunar lander assignment in the reinforcement learning course. As for hyperparameters, I tweaked the hidden layer size two layers of 10 nodes to try and make my neural network a bit more complex. I also used the "early\_stopping" option to allow the algorithm to finish sooner if it was ready, and changed to a constant learning rate, which would not decay as the model learned more and more. Figure 3 shows my results using all five original categories of data. It took about 10 minutes to run, and I am pretty pleased with the results!

Overall for neural network training, it seems like this model was able to scale to larger numbers of examples a lot better than decision trees, as shown in the second graph. In the third, it seems like we are also getting some better than linear performance expectations as far as time required to do queries for each of the training examples.


\section{Boosting}

The Boost learning technique is characterized by learning some "weak learner" techniques, and combining them to make "strong learners". It tends to be good at reducing bias and variance, and is a bit more resistant to overfitting than some other algorithms. For hyperparameters, I used 10 boosting stages (generally a low value), and a learning rate of 1.0 (a pretty high value). I used a min\_impurity\_decrease of 0.1 to help limit how long my algorithm would run. I also set the minimum weight fraction for any given leaf mode to 0.001, and minimum samples at any leaf node to 5 and try and prevent overfitting. 

The best boost learning run I performed took about 11 minutes to complete, which was definitely one of the longer-running techniques I did. My results are visible in Figure 4. Based on the scalability graph, it seems to scale pretty linearly over time based on adding more and more samples. For performance of the model, boost seems to behave very similarly to the decision tree technique, with not much more time required for lookups after the model has many samples in it. As for the learning curve, as one would expect, the training data is very good! Boost prioritizes learning training sets that were incorrect the last time, which explains why its training performance is so much better than its testing performance, unlike most of the other techniques. 

\begin{figure}
	\centering
	\includegraphics[width=1.3\linewidth]{images/./Boost10estimators.png}
	\caption{Boost network learning using 10 estimators}
\end{figure}

\section{Support Vector Machines}

For the support vector machine portion of the assignment, I once again used the Scikit Learn library. I was optimistic about this being the best of the techniques, because it handles high-dimensional spaces very well. For the data I am using, the text vectorization results in a lot of really sparse samples, which is something that SVM tends to be good at (high number of data dimensions compared to number of samples). 

I chose to use the C-Support Vector Classification algorithm (SVC), which allows for different kernels in the testing process. I ran this technique trying a few of the different kernel techniques, including linear, sigmoid, polynomial (3 degrees), and radial basis function. My results are visible in Figure 5. They varied wildly on how long they took, from just 10 minutes to nearly 30 minutes. They all seemed to have about the same scalability, but surprisingly the linear kernel had the best performance of all the techniques, getting something like .83 for its cross-training validation score at the end of testing, which is one of the best performers in my research! The SVM technique didn't scale very well during training, as all of the kernels I tried scaled polynomially with the number of training examples. For this reason, I would not continue to pursue SVM as a technique for datasets that have many examples. On the other hand, once the model is trained it seems to perform very well for lookup time, scaling at something approximating log(n) for test lookup time. Overall, SVM left me optimistic that with some more tuning, this would probably be the best technique.

\begin{figure}
	\centering
	\begin{subfigure}[b]{1.3\linewidth}
		\centering
		\includegraphics[width=\linewidth]{images/SVM_Linear.png}
		\caption{Linear kernel}
	\end{subfigure}
	\begin{subfigure}[b]{1.3\linewidth}
		\centering
		\includegraphics[width=\linewidth]{images/SVM_3poly.png}	
		\caption{3-polynomial kernel}		
	\end{subfigure}
	\begin{subfigure}[b]{1.3\linewidth}
		\centering
		\includegraphics[width=\linewidth]{images/SVM_RBF.png}	
		\caption{RBF kernel}		
	\end{subfigure}
	\caption{Different kernels used in SVM learning}
	
\end{figure}


\section{K-Nearest Neighbors}

Finally, for the k-nearest neighbors technique, I continued to use the Scikit Learn library. Here, I had some pretty bad results using a lot of the library defaults. I had to use the "brute force" distance algorithm, since I had such sparse input vectors that it would make tree or ball tree distance-finding algorithms not work well. On the other hand, I was able to use all of my processor cores because this algorithm can be run in parallel fairly easily (at least, on the finding of the neighbors by brute force).

This technique is a terrible choice for my data use-case, because it does not really "learn" in the sense that the other algoriths do. It simply stores the previous information without processing it, and attempts to look up which neighbors could give it the best hints about the output. KNN does not work well for processing of words in this fashion, because it does not predict which elements of the vector input (words) are most helpful at determining the predicted category. I included one typical result for the many different times I tried to tweak KNN to get it to work. The results are pretty dismal. I suspect that there is a different vectorization or tokenization technique I could use to make KNN work well with the 20 Newsgroups dataset, but the vectorization strategy I am using now is poorly suited to this technique.

Based on Figure 6, you can see that the technique did about as well as random. The scaling and performance do not really matter in this case because of this technique's lack of applicability to my dataset, but at least it seems to scale well and have nearly linear lookup times. This strikes me as odd, since I would expect the brute force lookup for the K nearest neighbors to scale exponentially as the number of training examples goes up, rather than linearly. On the other hand, the scalability of this model is extremely good, since it only has to take time to store the new data in the model, rather than modifying the entire model like other techniques have to do.

\begin{figure}
	\centering
	\includegraphics[width=1.3\linewidth]{images/./KNN_5.png}
	\caption{Boost network learning using 10 estimators}
\end{figure}

\section{Summary}

Based on my results for this assignment, the two best algorithms to use for problems similar to 20 Newsgroups classification are linear kernel SVM and decision tree learning techniques. Decision tree learning was quite a bit faster on wall-clock time than SVM, but SVM got slightly better results at the end of training, with nearly 0.85 correctness rate for the test data. The absolute worst technique used was KNN, which is simply not well-suited for the training data I chose.

Overall, I learned a lot about different machine learning techniques as a result of this assignment. In the future, were I to continue researching this dataset, I would probably search the space of hyperparameters for linear kernel SVM and decision trees to try and better optimize their run-time and correctness.


\end{document}

