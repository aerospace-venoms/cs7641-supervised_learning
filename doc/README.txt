Instructions for running my code:

git clone https://gitlab.com/aerospace-venoms/cs7641-supervised_learning.git

cd cs7641-supervised_learning
./ps1.py -h

Args after ./ps1.py: 
dt: Decision tree
nn: Neural network
boost: Boosting technique
svm: Support vector machine
knn: K-nearest neighbors

Example:
./ps1.py dt

