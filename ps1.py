#!/usr/bin/env python3
"""
Gatech OMSCS
CS7641: Machine Learning
Project 1: Supervised Learning
"""

# Includes for skeleton code
import sys
import os
import traceback
import argparse
import time
import logging

# Includes for graphing results
from sklearn.model_selection import learning_curve
from sklearn.model_selection import ShuffleSplit
import matplotlib.pyplot as plt
import graphviz
from pprint import pprint
import numpy as np

# Supervised learning techniques:
from sklearn import tree
from sklearn.neural_network import MLPClassifier
from sklearn.ensemble import GradientBoostingClassifier
from sklearn import svm
from sklearn.neighbors import KNeighborsClassifier

# create file handler which logs even debug messages
log = logging.getLogger()
log.setLevel(logging.ERROR)  # DEBUG | INFO | WARNING | ERROR | CRITICAL
formatter = logging.Formatter(
    "%(asctime)s - %(name)s - %(levelname)s - Line: %(lineno)d\n%(message)s"
)
sh = logging.StreamHandler()
sh.setLevel(logging.ERROR)
sh.setFormatter(formatter)
log.addHandler(sh)
fh = logging.FileHandler(
    os.path.abspath(
        os.path.join(
            os.path.dirname(__file__), os.path.basename(__file__).rstrip(".py") + ".log"
        )
    )
)
fh.setLevel(logging.ERROR)
fh.setFormatter(formatter)
log.addHandler(fh)

learning_routines = ["dt", "nn", "boost", "svm", "knn"]


def main():
    global args
    # Get the 20_newsgroups dataset
    X, y, target_names = get_dataset()
    # Default case: Decision tree
    clf = tree.DecisionTreeClassifier(max_depth=10)

    if args.learning_algo == "dt":
        # default case
        """
        This section borrows heavily from the example code at:
        https://scikit-learn.org/stable/modules/tree.html
        https://scikit-learn.org/stable/auto_examples/tree/plot_cost_complexity_pruning.html#sphx-glr-auto-examples-tree-plot-cost-complexity-pruning-py
        https://scikit-learn.org/0.19/datasets/twenty_newsgroups.html
        """
        print("Performing decision tree routine. (default case)")
        show_decision_tree(clf, X, y, target_names)

    if args.learning_algo == "nn":
        print("Performing neural network learning routine.")
        clf = MLPClassifier(
            solver="adam",
            alpha=0.0001,
            hidden_layer_sizes=(10, 10),
            random_state=1,
            max_iter=200,
            early_stopping=True,
            learning_rate="constant",
        )

    if args.learning_algo == "boost":
        print("Performing boost learning routine.")
        clf = GradientBoostingClassifier(
            n_estimators=100,
            learning_rate=0.1,
            min_impurity_decrease=0.1,
            max_depth=10,
            random_state=0,
            min_weight_fraction_leaf=0.001,
            min_samples_leaf=5,
        )

    if args.learning_algo == "svm":
        print("Performing Support Vector Machine learning routine.")
        clf = svm.SVC(kernel="poly", cache_size=400)

    if args.learning_algo == "knn":
        print("Performing K-Nearest Neighbors learning routine.")
        clf = KNeighborsClassifier(
            n_neighbors=5, weights="distance", algorithm="brute", n_jobs=-1
        )

    do_learning(clf, X, y, target_names)


def do_learning(clf, X, y, target_names):

    # Shuffle the data and get a learning curve for the data
    cv = ShuffleSplit(n_splits=100, test_size=0.2, random_state=0)
    plot_learning_curve(clf, "Learning Curves", X=X, y=y, cv=cv, n_jobs=8)
    plt.show()


def show_decision_tree(clf, X, y, target_names):

    # Perform the classifier so we can see the resultant graph (optional)
    clf = clf.fit(X, y)
    tree.plot_tree(clf)

    dot_data = tree.export_graphviz(clf, out_file=None)
    graph = graphviz.Source(dot_data, format="png")

    dot_data = tree.export_graphviz(
        clf,
        out_file=None,  # feature_names=my_dataset.feature_names,
        class_names=target_names,
        filled=True,
        rounded=True,
        special_characters=True,
    )
    graph = graphviz.Source(dot_data)
    graph.view()


def get_dataset():

    from sklearn.datasets import fetch_20newsgroups

    # Load the data
    my_dataset = fetch_20newsgroups(
        subset="train",
        data_home="./scikit_learn_data/",
        random_state=0,
        shuffle=True,
        remove=("headers", "footers", "quotes"),
        categories=[
            "rec.autos",
            "sci.crypt",
            "talk.religion.misc",
            "talk.politics.guns",
            "misc.forsale",
        ],
    )

    # Convert the text to vectors to aid in analysis:
    from sklearn.feature_extraction.text import TfidfVectorizer

    vectorizer = TfidfVectorizer()
    vectors = vectorizer.fit_transform(my_dataset.data)

    X = vectors
    y = my_dataset.target
    targetnames = my_dataset.target_names

    return X, y, targetnames


def plot_learning_curve(
    estimator,
    title,
    X,
    y,
    axes=None,
    ylim=None,
    cv=None,
    n_jobs=None,
    train_sizes=np.linspace(0.1, 1.0, 5),
):
    # I found this helpful code at: https://scikit-learn.org/stable/auto_examples/model_selection/plot_learning_curve.html
    """
    Generate 3 plots: the test and training learning curve, the training
    samples vs fit times curve, the fit times vs score curve.

    Parameters
    ----------
    estimator : estimator instance
        An estimator instance implementing `fit` and `predict` methods which
        will be cloned for each validation.

    title : str
        Title for the chart.

    X : array-like of shape (n_samples, n_features)
        Training vector, where ``n_samples`` is the number of samples and
        ``n_features`` is the number of features.

    y : array-like of shape (n_samples) or (n_samples, n_features)
        Target relative to ``X`` for classification or regression;
        None for unsupervised learning.

    axes : array-like of shape (3,), default=None
        Axes to use for plotting the curves.

    ylim : tuple of shape (2,), default=None
        Defines minimum and maximum y-values plotted, e.g. (ymin, ymax).

    cv : int, cross-validation generator or an iterable, default=None
        Determines the cross-validation splitting strategy.
        Possible inputs for cv are:

          - None, to use the default 5-fold cross-validation,
          - integer, to specify the number of folds.
          - :term:`CV splitter`,
          - An iterable yielding (train, test) splits as arrays of indices.

        For integer/None inputs, if ``y`` is binary or multiclass,
        :class:`StratifiedKFold` used. If the estimator is not a classifier
        or if ``y`` is neither binary nor multiclass, :class:`KFold` is used.

        Refer :ref:`User Guide <cross_validation>` for the various
        cross-validators that can be used here.

    n_jobs : int or None, default=None
        Number of jobs to run in parallel.
        ``None`` means 1 unless in a :obj:`joblib.parallel_backend` context.
        ``-1`` means using all processors. See :term:`Glossary <n_jobs>`
        for more details.

    train_sizes : array-like of shape (n_ticks,)
        Relative or absolute numbers of training examples that will be used to
        generate the learning curve. If the ``dtype`` is float, it is regarded
        as a fraction of the maximum size of the training set (that is
        determined by the selected validation method), i.e. it has to be within
        (0, 1]. Otherwise it is interpreted as absolute sizes of the training
        sets. Note that for classification the number of samples usually have
        to be big enough to contain at least one sample from each class.
        (default: np.linspace(0.1, 1.0, 5))
    """
    if axes is None:
        _, axes = plt.subplots(1, 3, figsize=(20, 5))

    axes[0].set_title(title)
    if ylim is not None:
        axes[0].set_ylim(*ylim)
    axes[0].set_xlabel("Training examples")
    axes[0].set_ylabel("Score")

    train_sizes, train_scores, test_scores, fit_times, _ = learning_curve(
        estimator,
        X,
        y,
        cv=cv,
        n_jobs=n_jobs,
        train_sizes=train_sizes,
        return_times=True,
    )
    train_scores_mean = np.mean(train_scores, axis=1)
    train_scores_std = np.std(train_scores, axis=1)
    test_scores_mean = np.mean(test_scores, axis=1)
    test_scores_std = np.std(test_scores, axis=1)
    fit_times_mean = np.mean(fit_times, axis=1)
    fit_times_std = np.std(fit_times, axis=1)

    # Plot learning curve
    axes[0].grid()
    axes[0].fill_between(
        train_sizes,
        train_scores_mean - train_scores_std,
        train_scores_mean + train_scores_std,
        alpha=0.1,
        color="r",
    )
    axes[0].fill_between(
        train_sizes,
        test_scores_mean - test_scores_std,
        test_scores_mean + test_scores_std,
        alpha=0.1,
        color="g",
    )
    axes[0].plot(
        train_sizes, train_scores_mean, "o-", color="r", label="Training score"
    )
    axes[0].plot(
        train_sizes, test_scores_mean, "o-", color="g", label="Cross-validation score"
    )
    axes[0].legend(loc="best")

    # Plot n_samples vs fit_times
    axes[1].grid()
    axes[1].plot(train_sizes, fit_times_mean, "o-")
    axes[1].fill_between(
        train_sizes,
        fit_times_mean - fit_times_std,
        fit_times_mean + fit_times_std,
        alpha=0.1,
    )
    axes[1].set_xlabel("Training examples")
    axes[1].set_ylabel("fit_times")
    axes[1].set_title("Scalability of the model")

    # Plot fit_time vs score
    axes[2].grid()
    axes[2].plot(fit_times_mean, test_scores_mean, "o-")
    axes[2].fill_between(
        fit_times_mean,
        test_scores_mean - test_scores_std,
        test_scores_mean + test_scores_std,
        alpha=0.1,
    )
    axes[2].set_xlabel("fit_times")
    axes[2].set_ylabel("Score")
    axes[2].set_title("Performance of the model")

    return plt


if __name__ == "__main__":

    try:
        start_time = time.time()
        parser = argparse.ArgumentParser(description=__doc__)
        parser.add_argument(
            "-v", "--verbose", action="store_true", default=False, help="verbose output"
        )
        parser.add_argument("-ver", "--version", action="version", version="1.0")
        parser.add_argument(
            "learning_algo",
            help="Algorithm to demonstrate. Valid entries are "
            + " ".join(learning_routines),
        )
        args = parser.parse_args()
        if args.verbose:
            fh.setLevel(logging.DEBUG)
            log.setLevel(logging.DEBUG)
        log.info("%s Started" % parser.prog)
        main()
        log.info("%s Ended" % parser.prog)
        log.info("Total running time in seconds: %0.2f" % (time.time() - start_time))
        sys.exit(0)
    except KeyboardInterrupt as e:  # Ctrl-C
        raise e
    except SystemExit as e:  # sys.exit()
        raise e
    except Exception as e:
        print("ERROR, UNEXPECTED EXCEPTION")
        print(str(e))
        traceback.print_exc()
        os._exit(1)
